###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

v0.4.0
******

Add a `transaction` context handler to propagate transaction IDs in logs for tracing transactions
Usage like `with transaction('My Command', parameters) as transaction_id:`.

v0.3.0
******

Change packaging to use "ska" namespace.
Usage like `from ska_logging import configure_logging` changes
to `from ska.logging import configure_logging`.

v0.2.1
******

Fix default formatter time.
Resulting output is now `1|2020-01-13T10:42:42.415Z|...` instead `1|2020-01-13 10:42:42,415.415Z|...`

v0.2.0
******

Provide access to default formatter.
The API is available via this public function `get_default_formatter`

v0.1.0
******

Added
-----

* First release of the Python package.
