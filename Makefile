# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

all: test lint

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
test:

	mkdir -p build && \
	python setup.py test | tee ./build/setup_py_test.stdout && \
	mv coverage.xml ./build/reports/code-coverage.xml

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
lint:

	# FIXME pylint needs to run twice since there is no way go gather the text and junit xml output at the same time
	# Also note that we are linting just the single configuration.py module, since pylint has import issues
	# if we call it with the src/ska/logging path - it can't find the standard library "logging".  Probably since
	# it is running in an importable folder with the same name.
	python3 -m pip install pylint_junit pylint2junit; \
	cd src; \
	pylint --output-format=parseable ska.logging.configuration | tee ../build/code_analysis.stdout; \
	pylint --output-format=pylint2junit.JunitReporter ska.logging.configuration > ../build/reports/linting.xml;


.PHONY: all test lint
