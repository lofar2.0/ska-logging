SKA Logging Configuration Library
=================================

[![Documentation Status](https://readthedocs.org/projects/ska-logging/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-logging/en/latest/?badge=latest)

This project allows standard logging configuration across all SKA projects.  The format used is described in detail
on the [developer portal](https://developer.skatelescope.org/en/latest/development/logging-format.html).

This library should be used to configure the application's logging system as early as possible at start up.
Note that for Python TANGO devices that inherit from [SKABaseDevice](https://gitlab.com/ska-telescope/lmc-base-classes/-/blob/master/src/ska/base/base_device.py),
this is already done in the base class, so it does not need to be done again.

Usage
-----

For Python applications, this is as simple as:

```python
import logging
from ska.logging import configure_logging

def main():
    configure_logging()
    logger = logging.getLogger("ska.example")
    logger.info("Logging started for Example application")
```
The `configure_logging` function takes additional arguments, including one that allows the initial log level to
be specified.  It may be useful to link that to a command line option or environment variable.

```python
import logging
from ska.logging import configure_logging

def main():
    configure_logging(logging.DEBUG)
```

SKA's logging format allows for simple tags (key-value pairs) to be included in log messages.  Application-specific
behaviour can be provided via a filter, which will be added to all handlers in the configuration used
by `configure_logging`.  If this filter is provided, it must add a `tags` attribute to each record, as the log
message formatting string will include a `%(tags)s` field.

Note that the logging format limits the characters allowed in tags.  For example, `|` is not allowed.
No validation is done by this library.  If the filter is `None` (the default), then the field will be omitted.

```python
import logging
from ska.logging import configure_logging


class TangoDeviceTagsFilter(logging.Filter):
    def filter(self, record):
        record.tags = "tango-device:my/tango/device"
        return True

def main():
    configure_logging(level="DEBUG", tags_filter=TangoDeviceTagsFilter)
```

In the more general case, the configuration can be updated with an additional dict, matching
the `logging.config.dictConfig`
[schema](https://docs.python.org/3/library/logging.config.html#dictionary-schema-details).
This additional dict is recursively merged with the default configuration.  While not recommended, keys in the
default configuration can be removed by setting the corresponding override key's value to `None`.   In the
example below, we add output to a file.  Note that the `"default"` formatter and `"console"` handler are provided
by the default configuration.

```python
import logging.handlers
from ska.logging import configure_logging


ADDITIONAL_LOGGING_CONFIG = {
    "handlers": {
        "file": {
            "()" : logging.handlers.RotatingFileHandler,
            "formatter": "default",
            "filename": "./ska.example.log",
            "maxBytes": 2048,
            "backupCount": 2,
        }
    },
    "root": {
        "handlers": ["console", "file"],
    }
}

def main():
    configure_logging(overrides=ADDITIONAL_LOGGING_CONFIG)
```

Custom handlers that use the standard logging format may be useful.  In this case, the function
`get_default_formatter` is available.  The example below is contrived, but shows the approach.
A more practical use case is adding and removing handlers at runtime.

```python
import logging
import logging.handlers
from ska.logging import configure_logging, get_default_formatter


def main():
    configure_logging()
    logger = logging.getLogger("ska.example")
    handler = logging.handlers.MemoryHandler(capacity=10)
    handler.setFormatter(get_default_formatter())
    logger.addHandler(handler)
    logger.info("Logging started for Example application")
```

By default, calls to `configure_logging` do not disable existing non-root loggers.  This allows
multiple calls to the function, although that will generally not be required.  This behaviour can be
overridden using the `"disable_existing_loggers"` key.

### Logging Transaction IDs

A transaction context handler is available to inject ids fetched from the the skuid service into logs. The transaction id will be logged on entry and exit of the context handler. In the event of an exception, the transaction id will be logged with the exception stack trace. The ID generated depends on whether or not the SKUID_URL environment variable is set.

```python
from ska.logging import transaction

def command(self, parameter_json):
    parameters = json.reads(parameter_json)
    with transaction('My Command', parameters) as transaction_id:
        # ...
        parameters['transaction_id'] = transaction_id
        device.further_command(json.dumps(parameters))
        # ...

```
The context handler logs to the local logger by default. Logs can be sent to a custom logger by passing a logger object as a keyword argument. Use `configure_logging` method for ska formatted logs.

**Example ska formatted logs for successful transaction**

Log message formats:

- On Entry:
  - Transaction[id]: Enter[name] with parameters [arguments] marker[marker]
- On Exit:
  - Transaction[id]: Exit[name] marker[marker]
- On exception:
  - Transaction[id]: Exception[name] marker[marker]
    -- Stacktrace --

The marker can be used to match entry/exception/exit log messages.

```txt

1|2020-10-01T12:49:31.119Z|INFO|Thread-210|thread_with_transaction_exception|test_transactions_threaded.py#23|transaction_id:txn-local-20201001-981667980|Transaction[txn-local-20201001-981667980]: Enter[Command] with parameters [{}] marker[52764]
1|2020-10-01T12:49:31.129Z|INFO|Thread-210|thread_with_transaction_exception|test_transactions_threaded.py#23|transaction_id:txn-local-20201001-981667980|Transaction[txn-local-20201001-981667980]: Exit[Command] marker[52764]

```

**Example ska formatted logs for failed transaction**

```txt
1|2020-10-01T12:51:35.588Z|INFO|Thread-204|thread_with_transaction_exception|test_transactions_threaded.py#23|transaction_id:txn-local-20201001-354400050|Transaction[txn-local-20201001-354400050]: Enter[Transaction thread [7]] with parameters [{}] marker[21454]
1|2020-10-01T12:51:35.598Z|ERROR|Thread-204|thread_with_transaction_exception|test_transactions_threaded.py#23|transaction_id:txn-local-20201001-354400050|Transaction[txn-local-20201001-354400050]: Exception[Transaction thread [7]] marker[21454]
Traceback (most recent call last):
  File "python_file.py", line 27, in thread_with_transaction_exception
    raise RuntimeError("An exception has occurred")
RuntimeError: An exception has occurred
1|2020-10-01T12:51:35.601Z|INFO|Thread-204|thread_with_transaction_exception|test_transactions_threaded.py#23|transaction_id:txn-local-20201001-354400050|Transaction[txn-local-20201001-354400050]: Exit[Transaction thread [7]] marker[21454]
```

Requirements
------------

The system used for development needs to have Python 3 and `pip` installed.

Install
-------

**Always** use a virtual environment. [Pipenv](https://pipenv.readthedocs.io/en/latest/) is now Python's officially
recommended method and the one used by default in this repo.

Follow these steps at the project root:

First, ensure that `~/.local/bin` is in your `PATH` with:
```bash
> echo $PATH
```

In case `~/.local/bin` is not part of your `PATH` variable, under Linux add it with:
```bash
> export PATH=~/.local/bin:$PATH
```
or the equivalent in your particular OS.

Then proceed to install pipenv and the required environment packages:

```bash
> pip install pipenv # if you don't have pipenv already installed on your system
> pipenv install
> pipenv shell
```

You will now be inside a pipenv shell with your virtual environment ready.

Use `exit` to exit the pipenv environment.


Testing
-------

* Put tests into the `tests` folder
* Use [PyTest](https://pytest.org) as the testing framework
  - Reference: [PyTest introduction](http://pythontesting.net/framework/pytest/pytest-introduction/)
* Run tests with `python setup.py test`
  - Configure PyTest in `setup.py` and `setup.cfg`
* Running the test creates the `htmlcov` folder
    - Inside this folder a rundown of the issues found will be accessible using the `index.html` file
* All the tests should pass before merging the code

Code analysis
-------------
 * Use [Pylint](https://www.pylint.org) as the code analysis framework
 * By default it uses the [PEP8 style guide](https://www.python.org/dev/peps/pep-0008/)
 * Use the provided `code-analysis.sh` script in order to run the code analysis in the `module` and `tests`
 * Code analysis should be run by calling `pylint ska.logging`. All pertaining options reside under the `.pylintrc` file.
 * Code analysis should only raise document related warnings (i.e. `#FIXME` comments) before merging the code

Writing documentation
---------------------
 * The documentation generator for this project is derived from SKA's [SKA Developer Portal repository](https://github.com/ska-telescope/developer.skatelescope.org)
 * The documentation can be edited under `./docs/src`
 * If you want to include only your README.md file, create a symbolic link inside the `./docs/src` directory if the existing one does not work:
 ```bash
$ cd docs/src
$ ln -s ../../README.md README.md
```
 * In order to build the documentation for this specific project, execute the following under `./docs`:
 ```bash
$ make html
```
* The documentation can then be consulted by opening the file `./docs/build/html/index.html`
