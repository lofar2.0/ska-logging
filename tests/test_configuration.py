# -*- coding: utf-8 -*-

"""Tests for the logging configuration module."""
import logging
import logging.handlers
import time

import pytest

import ska.logging.configuration as configuration

from ska.logging import configure_logging, get_default_formatter
from tests.conftest import get_named_handler, get_first_record_and_log_message


@pytest.mark.usefixtures("reset_logging")
class TestConfigureLogging:
    """Tests for :func:`~ska.logging.configuration.configure_logging`."""

    def test_includes_console_handler(self, default_logger):
        assert get_named_handler(default_logger, "console")

    def test_multiple_calls_non_root_logger_still_enabled(self, default_logger):
        logger = logging.getLogger("ska.logger.test")
        assert not logger.disabled
        configure_logging()
        assert not logger.disabled

    def test_default_log_level_info(self, default_logger):
        logger = logging.getLogger("ska.logger.test")
        assert logger.getEffectiveLevel() == logging.INFO
        assert default_logger.getEffectiveLevel() == logging.INFO

    def test_set_log_level_int(self):
        configure_logging(level=logging.DEBUG)
        logger = logging.getLogger("ska.logger.test")
        assert logger.getEffectiveLevel() == logging.DEBUG

    def test_set_log_level_string(self):
        configure_logging(level="WARNING")
        logger = logging.getLogger("ska.logger.test")
        assert logger.getEffectiveLevel() == logging.WARNING

    def test_default_uses_utc_time(self, recording_logger):
        recording_logger.info("UTC message")
        record, log_message = get_first_record_and_log_message(recording_logger)
        expected_time = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime(record.created))
        assert "UTC message" in log_message
        assert expected_time in log_message  # just testing UTC, so ignore the milliseconds part

    def test_default_no_tags(self, default_logger):
        handler = get_named_handler(default_logger, "console")
        formatter = handler.formatter
        assert "%(tag)s" not in formatter._fmt

    def test_tags_filter_adds_tags_field(self, recording_tags_logger):
        handler = get_named_handler(recording_tags_logger, "console")
        formatter = handler.formatter
        assert "%(tags)s" in formatter._fmt

    def test_tags_filter_emits_tags_value(self, recording_tags_logger):
        recording_tags_logger.info("Tags message")
        record, log_message = get_first_record_and_log_message(recording_tags_logger)
        assert record.tags == "key1:value1,key2:value2"
        assert log_message.endswith("|key1:value1,key2:value2|Tags message")

    def test_override(self):
        overrides = {
            "handlers": {"test": {"class": "logging.StreamHandler", "formatter": "default"}},
            "root": {"handlers": ["console", "test"]},
        }
        configure_logging(overrides=overrides)
        logger = logging.getLogger()
        assert get_named_handler(logger, "console")
        assert get_named_handler(logger, "test")


@pytest.mark.usefixtures("reset_logging")
class TestGetDefaultFormatter:
    """Tests for :func:`~ska.logging.configuration.get_default_formatter`."""

    def get_logged_test_message(self, logger):
        logger.info("test message")
        return get_first_record_and_log_message(logger)

    def test_default_no_tags(self, recording_logger):
        formatter = get_default_formatter()
        record, log_message = self.get_logged_test_message(recording_logger)
        actual_log_message = formatter.format(record)
        assert actual_log_message == log_message

    def test_get_tags_disabled(self, recording_logger):
        formatter = get_default_formatter(tags=False)
        record, log_message = self.get_logged_test_message(recording_logger)
        actual_log_message = formatter.format(record)
        assert actual_log_message == log_message

    def test_get_tags_enabled(self, recording_tags_logger):
        formatter = get_default_formatter(tags=True)
        record, log_message = self.get_logged_test_message(recording_tags_logger)
        actual_log_message = formatter.format(record)
        assert actual_log_message == log_message


class TestOverride:
    """Tests for :func:`~ska.logging.configuration._override`.

    Code based on:
        https://github.com/ska-sa/katsdpcontroller/blob/
        40ec9ae57adf1d441fc42b5c6eac86aa57b6e3aa/katsdpcontroller/test/test_product_config.py#L42

        Copyright (c) 2013-2019, National Research Foundation (SARAO)
        All rights reserved.
        (BSD-3-Clause license)
    """

    def test_add(self):
        out = configuration._override({"a": 1}, {"b": 2})
        assert out == {"a": 1, "b": 2}

    def test_remove(self):
        out = configuration._override({"a": 1, "b": 2}, {"b": None})
        assert out == {"a": 1}
        out = configuration._override(out, {"b": None})  # Already absent
        assert out == {"a": 1}

    def test_replace(self):
        out = configuration._override({"a": 1, "b": 2}, {"b": 3})
        assert out == {"a": 1, "b": 3}

    def test_recurse(self):
        orig = {"a": {"aa": 1, "ab": 2}, "b": {"ba": {"c": 10}, "bb": [5]}}
        override = {"a": {"aa": [], "ab": None, "ac": 3}, "b": {"bb": [1, 2]}}
        out = configuration._override(orig, override)
        assert out == {"a": {"aa": [], "ac": 3}, "b": {"ba": {"c": 10}, "bb": [1, 2]}}
