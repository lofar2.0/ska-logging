.. doctest-skip-all
.. _package-guide:

.. todo::
    - Insert todo's here

*****************
API documentation
*****************

This section details the public API for configuring application logging across the SKA project.

Python
======

The API for the configuration using Python is shown below.

.. Automatic API Documentation section. Generating the API from the docstrings. Modify / change the directory structure as you like

Public API Documentation
````````````````````````
Functions
---------

.. automodule:: ska.logging
    :members:
