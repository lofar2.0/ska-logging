# GitLab CI in conjunction with GitLab Runner can use Docker Engine to test and build any application.
# Docker, when used with GitLab CI, runs each job in a separate and isolated container using the predefined image that is set up in .gitlab-ci.yml.
# In this case we use the latest python docker image to build and test this project.
image: nexus.engageska-portugal.pt/ska-docker/ska-python-buildenv:latest

# cache is used to specify a list of files and directories which should be cached between jobs. You can only use paths that are within the project workspace.
# If cache is defined outside the scope of jobs, it means it is set globally and all jobs will use that definition
cache:
  paths:
# before_script is used to define the command that should be run before all jobs, including deploy jobs, but after the restoration of artifacts.
# This can be an array or a multi-line string.
before_script:
  - python3 -m pip install pipenv
  - pipenv install

stages:
  - build
  - test
  - linting
  - publish
  - deploy

# The YAML file defines a set of jobs with constraints stating when they should be run.
# You can specify an unlimited number of jobs which are defined as top-level elements with an arbitrary name and always
#  have to contain at least the script clause.

build wheel:
  stage: build
  tags:
    - docker-executor
  script:
    - python3 setup.py sdist bdist_wheel
  artifacts:
    paths:
      - ./dist/

# The test job produces a coverage report and the unittest output (see setup.cfg), and
#  the coverage xml report is moved to the reports directory while the html output is persisted for use by the pages
#  job. TODO: possibly a candidate for refactor / renaming later on.
test:
  stage: test
#  tags:
#   - docker-executor
  script:
   - pipenv run python3 setup.py test
   - mv coverage.xml ./build/reports/code-coverage.xml
  artifacts:
    paths:
    - ./build
    - htmlcov

list_dependencies:
  stage: test
  script:
    - pipenv graph >> pipenv_deps.txt
    - dpkg -l >> system_deps.txt
    - awk 'FNR>5 {print $2 ", " $3}' system_deps.txt >> system_deps.csv
    - mkdir .public
    - cp pipenv_deps.txt .public/
    - cp system_deps.txt .public/
    - cp system_deps.csv .public/
    - mv .public public
  artifacts:
    paths:
      - public

linting:
  image: nexus.engageska-portugal.pt/ska-docker/ska-python-buildenv:latest
  tags:
    - docker-executor
  stage: linting
  script:
    - make lint
  when: always
  artifacts:
    paths:
      - ./build

publish to nexus:
  stage: publish
  tags:
    - docker-executor
  variables:
    TWINE_USERNAME: $TWINE_USERNAME
    TWINE_PASSWORD: $TWINE_PASSWORD
  script:
    # Note: katversion will include the git hash in the version for non-tagged commits, but need git
    # Check metadata requirements
    - scripts/validate-metadata.sh
    - python3 -m pip install twine
    - twine upload --repository-url $PYPI_REPOSITORY_URL dist/*
  only:
    refs:
      - tags
    variables:
      # Confirm tag message exists
      - $CI_COMMIT_MESSAGE =~ /^.+$/
      # Confirm semantic versioning of tag
      - $CI_COMMIT_TAG =~ /^((([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?)(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?)$/

pages:
  stage: deploy
  tags:
   - docker-executor
  dependencies:
    - test
  script:
   - ls -la
   - mkdir .public
   - cp -r htmlcov/* .public
   - rm -rf htmlcov
   - mv .public public
  artifacts:
    paths:
      - public
    expire_in: 30 days

create ci metrics:
  stage: .post
  image: nexus.engageska-portugal.pt/ska-docker/ska-python-buildenv:latest
  when: always
  tags:
    - docker-executor
  script:
    # Gitlab CI badges creation: START
    - apt-get -y update
    - apt-get install -y curl --no-install-recommends
    - curl -s https://gitlab.com/ska-telescope/ci-metrics-utilities/raw/master/scripts/ci-badges-func.sh | sh
    # Gitlab CI badges creation: END
  artifacts:
    paths:
      - ./build
